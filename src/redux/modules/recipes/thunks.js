import apiClient from 'src/apiClient'
import { getAuthToken } from 'src/redux/modules/users/selectors'
import actions from './actions'

export const fetchRecipes = () => async (dispatch, getState) => {
  const authToken = getAuthToken(getState())
  dispatch(actions.getRecipes())
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    const { data } = await apiClient.get('./recipes', { headers })
    dispatch(actions.getRecipesSuccess(data.recipes))
  } catch (err) {
    dispatch(actions.getRecipesFail(err))
  }
}

export const createRecipe = (recipe) => async (dispatch, getState) => {
  const authToken = getAuthToken(getState())
  dispatch(actions.createRecipe(recipe))
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    const { data } = await apiClient.post('./recipes', recipe, { headers })
    dispatch(actions.createRecipesSuccess(data))
  } catch (err) {
    dispatch(actions.createRecipeFail(err))
  }
}

export const beginEditRecipe = recipeId => dispatch => {
  dispatch(actions.beginEditRecipe(recipeId))
}

export const finishEditRecipe = () => dispatch => {
  dispatch(actions.finishEditRecipe())
}

export const removeRecipe = (recipeId) => async (
  dispatch,
  getState
) => {
  const authToken = getAuthToken(getState())
  const originalRecipes = getState().recipes.recipes
  dispatch(actions.removeRecipe(recipeId))
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    await apiClient.delete(`./recipes/${recipeId}`, { headers })
    dispatch(actions.removeRecipeSuccess())
  } catch (err) {
    dispatch(actions.removeRecipeFail(err, originalRecipes))
  }
}

export const updateRecipe = (recipe) => async (
  dispatch,
  getState
) => {
  const authToken = getAuthToken(getState())
  const originalRecipes = getState().recipes.recipes
  dispatch(actions.updateRecipe(recipe))
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    await apiClient.put(`./recipes/${recipe.id}`, recipe, {
      headers,
    })
    dispatch(actions.updateRecipeSuccess())
    dispatch(actions.finishEditRecipe())
  } catch (err) {
    dispatch(actions.updateRecipeFail(err, originalRecipes))
  }
}

export default {
  beginEditRecipe,
  createRecipe,
  fetchRecipes,
  finishEditRecipe,
  removeRecipe,
  updateRecipe,
}
