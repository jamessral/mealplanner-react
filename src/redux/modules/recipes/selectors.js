import { createSelector } from 'reselect'

export const getModule = state => state.recipes

export const getRecipes = createSelector(getModule, recipes => recipes.recipes)

export const getIsEditing = createSelector(
  getModule,
  recipes => !!recipes.editing
)

export const getIsLoading = createSelector(
  getModule,
  recipes => recipes.loading
)

export default {
  getRecipes,
  getIsEditing,
  getIsLoading,
}
