import { getById, removeById, updateById } from 'src/redux/utils'
import {
  GET_RECIPES,
  GET_RECIPES_FAIL,
  GET_RECIPES_SUCCESS,
  CREATE_RECIPE,
  CREATE_RECIPE_FAIL,
  CREATE_RECIPE_SUCCESS,
  BEGIN_EDIT_RECIPE,
  FINISH_EDIT_RECIPE,
  UPDATE_RECIPE,
  UPDATE_RECIPE_FAIL,
  UPDATE_RECIPE_SUCCESS,
  REMOVE_RECIPE,
  REMOVE_RECIPE_FAIL,
  REMOVE_RECIPE_SUCCESS,
} from './types'

const initialState = {
  creating: false,
  createError: false,
  editing: null,
  recipes: [],
  loading: false,
  loadError: false,
  removing: false,
  removeError: false,
  updating: false,
  updateError: false,
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case GET_RECIPES:
      return {
        ...state,
        loading: true,
        loadError: null,
      }
    case GET_RECIPES_FAIL:
      return {
        ...state,
        loadError: action.error,
        loading: false,
      }
    case GET_RECIPES_SUCCESS:
      return {
        ...state,
        recipes: action.recipes,
        loading: false,
      }
    case CREATE_RECIPE:
      return {
        ...state,
        createError: null,
        creating: true,
      }
    case CREATE_RECIPE_FAIL:
      return {
        ...state,
        createError: action.error,
        creating: false,
      }
    case CREATE_RECIPE_SUCCESS:
      return {
        ...state,
        creating: false,
      }
    case BEGIN_EDIT_RECIPE:
      return {
        ...state,
        editing: getById(state.recipes, action.recipeId),
      }
    case FINISH_EDIT_RECIPE:
      return {
        ...state,
        editing: null,
      }
    case UPDATE_RECIPE: {
      const id = action.recipe.id
      return {
        ...state,
        recipes: updateById(state.recipes, id, action.recipe),
        updateError: null,
        updating: true,
      }
    }
    case UPDATE_RECIPE_FAIL:
      return {
        ...state,
        recipes: action.recipes,
        updateError: action.error,
        updating: false,
      }
    case UPDATE_RECIPE_SUCCESS:
      return {
        ...state,
        updating: false,
      }
    case REMOVE_RECIPE:
      return {
        ...state,
        recipes: removeById(state.recipes, action.recipeId),
        removeError: null,
        removing: true,
      }
    case REMOVE_RECIPE_FAIL:
      return {
        ...state,
        recipes: action.recipes,
        removeError: action.error,
        removing: false,
      }
    case REMOVE_RECIPE_SUCCESS:
      return {
        ...state,
        removing: false,
      }
    default:
      return state
  }
}
