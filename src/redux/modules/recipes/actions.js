import {
  GET_RECIPES,
  GET_RECIPES_FAIL,
  GET_RECIPES_SUCCESS,
  CREATE_RECIPE,
  CREATE_RECIPE_FAIL,
  CREATE_RECIPE_SUCCESS,
  BEGIN_EDIT_RECIPE,
  FINISH_EDIT_RECIPE,
  UPDATE_RECIPE,
  UPDATE_RECIPE_FAIL,
  UPDATE_RECIPE_SUCCESS,
  REMOVE_RECIPE,
  REMOVE_RECIPE_FAIL,
  REMOVE_RECIPE_SUCCESS,
} from './types'

export default {
  getRecipes: () => ({ type: GET_RECIPES }),
  getRecipesFail: error => ({
    error,
    type: GET_RECIPES_FAIL,
  }),
  getRecipesSuccess: recipes => ({
    recipes,
    type: GET_RECIPES_SUCCESS,
  }),
  createRecipe: recipe => ({
    recipe,
    type: CREATE_RECIPE,
  }),
  createRecipeFail: error => ({
    error,
    type: CREATE_RECIPE_FAIL,
  }),
  createRecipeSuccess: () => ({ type: CREATE_RECIPE_SUCCESS }),
  beginEditRecipe: recipeId => ({
    recipeId,
    type: BEGIN_EDIT_RECIPE,
  }),
  finishEditRecipe: () => ({
    type: FINISH_EDIT_RECIPE,
  }),
  updateRecipe: recipe => ({
    recipe,
    type: UPDATE_RECIPE,
  }),
  updateRecipeFail: (error, recipes) => ({
    error,
    recipes,
    type: UPDATE_RECIPE_FAIL,
  }),
  updateRecipeSuccess: () => ({ type: UPDATE_RECIPE_SUCCESS }),
  removeRecipe: recipeId => ({
    recipeId,
    type: REMOVE_RECIPE,
  }),
  removeRecipeFail: (error, recipes) => ({
    error,
    recipes,
    type: REMOVE_RECIPE_FAIL,
  }),
  removeRecipeSuccess: () => ({ type: REMOVE_RECIPE_SUCCESS }),
}
