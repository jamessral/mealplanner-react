import { createSelector } from 'reselect'

export const getModule = state => state.ingredients

export const getIngredients = createSelector(
  getModule,
  ingredients => ingredients.ingredients
)

export const getIsEditing = createSelector(
  getModule,
  ingredients => !!ingredients.editing
)

export const getIsLoading = createSelector(
  getModule,
  ingredients => ingredients.loading
)

export default {
  getIngredients,
  getIsEditing,
  getIsLoading,
}
