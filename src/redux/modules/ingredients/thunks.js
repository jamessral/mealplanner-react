import apiClient from 'src/apiClient'
import actions from './actions'

export const fetchIngredients = authToken => async dispatch => {
  dispatch(actions.getIngredients())
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    const { data } = await apiClient.get('./ingredients', { headers })
    dispatch(actions.getIngredientsSuccess(data.ingredients))
  } catch (err) {
    dispatch(actions.getIngredientsFail(err))
  }
}

export const createIngredient = (ingredient, authToken) => async dispatch => {
  dispatch(actions.createIngredient(ingredient))
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    const { data } = await apiClient.post('./ingredients', ingredient, { headers })
    dispatch(actions.createIngredientSuccess(data.ingredient))
  } catch (err) {
    dispatch(actions.createIngredientFail(err))
  }
}

export const beginEditIngredient = ingredientId => dispatch => {
  dispatch(actions.beginEditIngredient(ingredientId))
}

export const finishEditIngredient = () => dispatch => {
  dispatch(actions.finishEditIngredient())
}

export const removeIngredient = (ingredientId, authToken) => async (
  dispatch,
  getState
) => {
  const originalIngredients = getState().ingredients.ingredients
  dispatch(actions.removeIngredient(ingredientId))
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    await apiClient.delete(`./ingredients/${ingredientId}`, { headers })
    dispatch(actions.removeIngredientSuccess())
  } catch (err) {
    dispatch(actions.removeIngredientFail(err, originalIngredients))
  }
}

export const updateIngredient = (ingredient, authToken) => async (
  dispatch,
  getState
) => {
  const originalIngredients = getState().ingredients.ingredients
  dispatch(actions.updateIngredient(ingredient))
  const headers = {
    Authorization: `Bearer ${authToken}`,
  }
  try {
    await apiClient.put(`./ingredients/${ingredient.id}`, ingredient, {
      headers,
    })
    dispatch(actions.updateIngredientSuccess())
    dispatch(actions.finishEditIngredient())
  } catch (err) {
    dispatch(actions.updateIngredientFail(err, originalIngredients))
  }
}

export default {
  beginEditIngredient,
  createIngredient,
  fetchIngredients,
  finishEditIngredient,
  removeIngredient,
  updateIngredient,
}
