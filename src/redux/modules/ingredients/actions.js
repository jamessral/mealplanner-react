import {
  GET_INGREDIENTS,
  GET_INGREDIENTS_FAIL,
  GET_INGREDIENTS_SUCCESS,
  CREATE_INGREDIENT,
  CREATE_INGREDIENT_FAIL,
  CREATE_INGREDIENT_SUCCESS,
  BEGIN_EDIT_INGREDIENT,
  FINISH_EDIT_INGREDIENT,
  UPDATE_INGREDIENT,
  UPDATE_INGREDIENT_FAIL,
  UPDATE_INGREDIENT_SUCCESS,
  REMOVE_INGREDIENT,
  REMOVE_INGREDIENT_FAIL,
  REMOVE_INGREDIENT_SUCCESS,
} from './types'

export default {
  getIngredients: () => ({ type: GET_INGREDIENTS }),
  getIngredientsFail: error => ({
    error,
    type: GET_INGREDIENTS_FAIL,
  }),
  getIngredientsSuccess: ingredients => ({
    ingredients,
    type: GET_INGREDIENTS_SUCCESS,
  }),
  createIngredient: ingredient => ({
    ingredient,
    type: CREATE_INGREDIENT,
  }),
  createIngredientFail: error => ({
    error,
    type: CREATE_INGREDIENT_FAIL,
  }),
  createIngredientSuccess: () => ({ type: CREATE_INGREDIENT_SUCCESS }),
  beginEditIngredient: ingredientId => ({
    ingredientId,
    type: BEGIN_EDIT_INGREDIENT,
  }),
  finishEditIngredient: () => ({
    type: FINISH_EDIT_INGREDIENT,
  }),
  updateIngredient: ingredient => ({
    ingredient,
    type: UPDATE_INGREDIENT,
  }),
  updateIngredientFail: (error, ingredients) => ({
    error,
    ingredients,
    type: UPDATE_INGREDIENT_FAIL,
  }),
  updateIngredientSuccess: () => ({ type: UPDATE_INGREDIENT_SUCCESS }),
  removeIngredient: ingredientId => ({
    ingredientId,
    type: REMOVE_INGREDIENT,
  }),
  removeIngredientFail: (error, ingredients) => ({
    error,
    ingredients,
    type: REMOVE_INGREDIENT_FAIL,
  }),
  removeIngredientSuccess: () => ({ type: REMOVE_INGREDIENT_SUCCESS }),
}
