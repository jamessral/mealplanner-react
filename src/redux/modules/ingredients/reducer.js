import { getById, removeById, updateById } from 'src/redux/utils'
import {
  GET_INGREDIENTS,
  GET_INGREDIENTS_FAIL,
  GET_INGREDIENTS_SUCCESS,
  CREATE_INGREDIENT,
  CREATE_INGREDIENT_FAIL,
  CREATE_INGREDIENT_SUCCESS,
  BEGIN_EDIT_INGREDIENT,
  FINISH_EDIT_INGREDIENT,
  UPDATE_INGREDIENT,
  UPDATE_INGREDIENT_FAIL,
  UPDATE_INGREDIENT_SUCCESS,
  REMOVE_INGREDIENT,
  REMOVE_INGREDIENT_FAIL,
  REMOVE_INGREDIENT_SUCCESS,
} from './types'

const initialState = {
  creating: false,
  createError: false,
  editing: null,
  ingredients: [],
  loading: false,
  loadError: false,
  removing: false,
  removeError: false,
  updating: false,
  updateError: false,
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case GET_INGREDIENTS:
      return {
        ...state,
        loading: true,
        loadError: null,
      }
    case GET_INGREDIENTS_FAIL:
      return {
        ...state,
        loadError: action.error,
        loading: false,
      }
    case GET_INGREDIENTS_SUCCESS:
      return {
        ...state,
        ingredients: action.ingredients,
        loading: false,
      }
    case CREATE_INGREDIENT:
      return {
        ...state,
        createError: null,
        creating: true,
      }
    case CREATE_INGREDIENT_FAIL:
      return {
        ...state,
        createError: action.error,
        creating: false,
      }
    case CREATE_INGREDIENT_SUCCESS:
      return {
        ...state,
        creating: false,
      }
    case BEGIN_EDIT_INGREDIENT:
      return {
        ...state,
        editing: getById(state.ingredients, action.ingredientId),
      }
    case FINISH_EDIT_INGREDIENT:
      return {
        ...state,
        editing: null,
      }
    case UPDATE_INGREDIENT: {
      const id = action.ingredient.id
      return {
        ...state,
        ingredients: updateById(state.ingredients, id, action.ingredient),
        updateError: null,
        updating: true,
      }
    }
    case UPDATE_INGREDIENT_FAIL:
      return {
        ...state,
        ingredients: action.ingredients,
        updateError: action.error,
        updating: false,
      }
    case UPDATE_INGREDIENT_SUCCESS:
      return {
        ...state,
        updating: false,
      }
    case REMOVE_INGREDIENT:
      return {
        ...state,
        ingredients: removeById(state.ingredients, action.ingredientId),
        removeError: null,
        removing: true,
      }
    case REMOVE_INGREDIENT_FAIL:
      return {
        ...state,
        ingredients: action.ingredients,
        removeError: action.error,
        removing: false,
      }
    case REMOVE_INGREDIENT_SUCCESS:
      return {
        ...state,
        removing: false,
      }
    default:
      return state
  }
}
