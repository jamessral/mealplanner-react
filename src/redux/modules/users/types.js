export const SET_AUTH_TOKEN = 'users/setAuthToken'

export const SIGN_IN_USER = 'users/signIn'
export const SIGN_IN_USER_FAIL = 'users/signInFail'
export const SIGN_IN_USER_SUCCESS = 'users/signInSuccess'

export const SIGN_OUT_USER = 'users/signOut'

export const SIGN_UP_USER = 'users/signUp'
export const SIGN_UP_USER_FAIL = 'users/signUpFail'
export const SIGN_UP_USER_SUCCESS = 'users/signUpSuccess'

export default {
  SET_AUTH_TOKEN,
  SIGN_IN_USER,
  SIGN_IN_USER_FAIL,
  SIGN_IN_USER_SUCCESS,
  SIGN_OUT_USER,
  SIGN_UP_USER,
  SIGN_UP_USER_FAIL,
  SIGN_UP_USER_SUCCESS,
}
