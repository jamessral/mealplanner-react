import apiClient from 'src/apiClient'
import {
  navigate
} from '@reach/router'
import actions from './actions'

export const setAuthToken = token => async dispatch => {
  dispatch(actions.setAuthToken(token))
}

export const signIn = (email, password) => async dispatch => {
  dispatch(actions.signInUser(email, password))
  try {
    const { data } = await apiClient.post('./users/signIn', {
      email,
      password
    })
    dispatch(actions.signInUserSuccess(email, data.token))
    await window.localStorage.setItem('authToken', data.token)
    navigate('/')
  } catch (err) {
    const error = err.response ? err.response.data.error : err
    dispatch(actions.signInUserFail(error))
  }
}

export const signUp = (
  email,
  password,
  passwordConfirmation
) => async dispatch => {
  dispatch(actions.signUpUser(email, password, passwordConfirmation))
  try {
    const { data } = await apiClient.post('./users/signUp', {
      email,
      password,
      passwordConfirmation,
    })
    dispatch(actions.signUpUserSuccess(email, data.token))
    navigate('/sign_in')
  } catch (err) {
    dispatch(actions.signUpUserFail(err))
  }
}

export const signOut = () => async dispatch => {
  dispatch(actions.signOutUser())
  try {
    await window.localStorage.removeItem('authToken')
  } catch (err) {
    console.log(`There was an error setting local storage: ${err.message}`)
  }
  navigate('/')
}

export default {
  signIn,
  signOut,
  signUp,
}
