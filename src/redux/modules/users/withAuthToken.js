import { connect } from 'react-redux'
import { getAuthToken } from 'src/redux/modules/users/selectors'

const mapStateToProps = state => ({
  authToken: getAuthToken(state),
})

export default WrappedComponent =>
  connect(
    mapStateToProps,
    {}
  )(WrappedComponent)
