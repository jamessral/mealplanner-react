import {
  SET_AUTH_TOKEN,
  SIGN_IN_USER,
  SIGN_IN_USER_FAIL,
  SIGN_IN_USER_SUCCESS,
  SIGN_OUT_USER,
  SIGN_UP_USER,
  SIGN_UP_USER_FAIL,
  SIGN_UP_USER_SUCCESS,
} from './types'

export default {
  setAuthToken: token => ({
    token,
    type: SET_AUTH_TOKEN,
  }),
  signInUser: (email, password) => ({
    email,
    password,
    type: SIGN_IN_USER,
  }),
  signInUserFail: error => ({
    error,
    type: SIGN_IN_USER_FAIL,
  }),
  signInUserSuccess: (email, token) => ({
    email,
    token,
    type: SIGN_IN_USER_SUCCESS,
  }),
  signOutUser: () => ({ type: SIGN_OUT_USER }),
  signUpUser: (email, password, passwordConfirmation) => ({
    email,
    password,
    passwordConfirmation,
    type: SIGN_UP_USER,
  }),
  signUpUserFail: error => ({
    error,
    type: SIGN_UP_USER_FAIL,
  }),
  signUpUserSuccess: (email, token) => ({
    email,
    token,
    type: SIGN_UP_USER_SUCCESS,
  }),
}
