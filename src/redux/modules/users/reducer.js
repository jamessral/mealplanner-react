import {
  SET_AUTH_TOKEN,
  SIGN_IN_USER,
  SIGN_IN_USER_FAIL,
  SIGN_IN_USER_SUCCESS,
  SIGN_OUT_USER,
  SIGN_UP_USER,
  SIGN_UP_USER_FAIL,
  SIGN_UP_USER_SUCCESS,
} from './types'

const initialState = {
  authToken: null,
  loadingSignIn: false,
  loadingSignUp: false,
  signInError: null,
  signUpError: null,
}

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case SET_AUTH_TOKEN:
      return {
        ...state,
        authToken: action.token
      }
    case SIGN_IN_USER:
      return {
        ...state,
        loadingSignIn: true,
        signInError: null,
      }
    case SIGN_IN_USER_FAIL:
      return {
        ...state,
        loadingSignIn: false,
        signInError: action.error,
      }
    case SIGN_IN_USER_SUCCESS:
      return {
        ...state,
        loadingSignIn: false,
        authToken: action.token,
      }
    case SIGN_UP_USER:
      return {
        ...state,
        loadingSignUp: true,
        signUpError: null,
      }

    case SIGN_UP_USER_FAIL:
      return {
        ...state,
        loadingSignUp: false,
        signUpError: action.error,
      }
    case SIGN_UP_USER_SUCCESS:
      return {
        ...state,
        loadingSignUp: false,
        authToken: action.token,
      }
    case SIGN_OUT_USER:
      return {
        ...state,
        authToken: null,
      }
    default:
      return state
  }
}
