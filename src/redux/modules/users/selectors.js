import { createSelector } from 'reselect'

const users = state => state.users

export const getIsSignedIn = createSelector(users, state =>
  Boolean(state.authToken)
)

export const getAuthToken = createSelector(users, state => state.authToken)

export const getSignInError = createSelector(users, state => state.signInError)

export const getSignUpError = createSelector(users, state => state.signUpError)

export default {
  getAuthToken,
  getIsSignedIn,
  getSignInError,
  getSignUpError,
}
