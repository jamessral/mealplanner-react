import React from 'react'
import PropTypes from 'prop-types'

const SubmitButtons = ({ onCancel, onSubmit, disabled }) => (
  <div className="flex items-center justify-between my-4">
    <button className="btn btn-grey px-8 text-grey-lightest" onClick={onCancel}>
      Cancel
    </button>
    <button
      className="btn btn-blue px-8"
      onClick={onSubmit}
      disabled={disabled}
      type="submit"
    >
      Submit
    </button>
  </div>
)

SubmitButtons.propTypes = {
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  disabled: PropTypes.bool,
}

SubmitButtons.defaultProps = {
  onCancel() {},
  onSubmit() {},
}

export default SubmitButtons
