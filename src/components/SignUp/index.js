import { connect } from 'react-redux'
import { signUp } from 'src/redux/modules/users/thunks'
import { selectors as userSelectors } from 'src/redux/modules/users'
import SignUp from './SignUp'

const mapStateToProps = state => ({
  signUpError: userSelectors.getSignUpError(state),
})

const mapDispatchToProps = {
  signUp,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp)
