import React from 'react'
import PropTypes from 'prop-types'
import SubmitButtons from 'src/components/SubmitButtons'
import { Link } from 'gatsby'

const initialState = {
  email: '',
  password: '',
  passwordConfirmation: '',
  valid: false,
}

export default class SignUpForm extends React.PureComponent {
  state = initialState

  static propTypes = {
    signUp: PropTypes.func.isRequired,
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress)
  }

  componentWillUnmount() {
    document.addEventListener('keydown', this.handleKeyPress)
  }

  get isEmailValid() {
    const { email } = this.state

    return email && email.length > 3 && email.match(/\w+@\w+\.\w+/)
  }

  get isPasswordValid() {
    const { password } = this.state

    return password && password.length > 3
  }

  get isPasswordConfirmationValid() {
    const { password, passwordConfirmation } = this.state

    return (
      passwordConfirmation &&
      passwordConfirmation.length > 3 &&
      passwordConfirmation === password
    )
  }

  get isFormValid() {
    return (
      this.isEmailValid &&
      this.isPasswordValid &&
      this.isPasswordConfirmationValid
    )
  }

  handleCancel = event => {
    console.log(event)
    event.preventDefault()
    this.setState({ ...initialState })
  }

  handleFieldChange = event => {
    const name = event.target.name
    const value = event.target.value

    this.setState({ [name]: value })
  }

  handleKeyPress = (event) => {
    // Enter key
    if (event.keyCode === 13) {
      this.handleSubmit(event)
    }
  }

  handleSubmit = event => {
    event.preventDefault()
    const { email, password, passwordConfirmation } = this.state

    if (!this.isFormValid) { return }

    this.props.signUp(email, password, passwordConfirmation)
    this.setState({ ...initialState })
  }

  render() {
    const { email, password, passwordConfirmation } = this.state

    return (
      <div className="form-container">
        <h3 className="pb-4">Sign Up</h3>
        <form>
          <label htmlFor="email">Email</label>
          <input
            className="form-field"
            type="email"
            name="email"
            value={email}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="password">Password</label>
          <input
            className="form-field"
            type="password"
            name="password"
            value={password}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="passwordConfirmation">Confirm Password</label>
          <input
            className="form-field"
            type="password"
            name="passwordConfirmation"
            value={passwordConfirmation}
            onChange={this.handleFieldChange}
          />
          <SubmitButtons
            onCancel={this.handleCancel}
            onSubmit={this.handleSubmit}
            disabled={!this.isFormValid}
          />
        </form>
        <div className="flex flex-row justify-between items-center mt-4">
          <span>Already have an account?</span>
          <Link className="text-blue text-base font-bold" to="/sig_in">
            Sign In
          </Link>
        </div>
      </div>
    )
  }
}
