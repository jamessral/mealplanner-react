import { connect } from 'react-redux'
import {
  beginEditIngredient,
  removeIngredient,
  updateIngredient,
} from 'src/redux/modules/ingredients/thunks'
import IngredientItem from './IngredientItem'

const mapStateToProps = state => ({})

const mapDispatchToProps = {
  beginEditIngredient,
  removeIngredient,
  updateIngredient,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IngredientItem)
