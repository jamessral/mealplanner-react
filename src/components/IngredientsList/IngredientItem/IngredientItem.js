import React from 'react'
import PropTypes from 'prop-types'

class IngredientItem extends React.Component {
  static propTypes = {
    beginEditIngredient: PropTypes.func.isRequired,
    ingredient: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string,
      price: PropTypes.number,
      unit: PropTypes.string,
    }).isRequired,
    removeIngredient: PropTypes.func.isRequired,
  }

  handleDelete = () => {
    const { ingredient, removeIngredient } = this.props

    removeIngredient(ingredient.id)
  }

  handleEdit = () => {
    const { beginEditIngredient, ingredient } = this.props
    beginEditIngredient(ingredient.id)
  }

  render() {
    const { ingredient } = this.props
    return (
      <div className="flex flex-col justify-center px-4 py-4 shadow-md rounded">
        <div className="flex justify-between">
          <div className="text-grey-darkest text-lg font-bold py-2">
            Name:{' '}
            <span className="font-normal text-base">{ingredient.name}</span>
          </div>
          <button className="text-grey-dark" onClick={this.handleDelete}>
            X
          </button>
        </div>
        <div className="text-grey-darkest text-lg font-bold py-2">
          Description:{' '}
          <span className="font-normal text-base">
            {ingredient.description}
          </span>
        </div>
        <div className="text-grey-darkest text-lg font-bold py-2">
          Price:{' '}
          <span className="font-normal text-base">{ingredient.price}</span>
        </div>
        <div className="text-grey-darkest text-lg font-bold py-2">
          Unit: <span className="font-normal text-base">{ingredient.unit}</span>
        </div>
        <button onClick={this.handleEdit} className="btn btn-blue-rev">
          Edit
        </button>
      </div>
    )
  }
}

export default IngredientItem
