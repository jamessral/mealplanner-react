import React from 'react'
import PropTypes from 'prop-types'
import IngredientItem from './IngredientItem'

class IngredientsList extends React.Component {
  static propTypes = {
    authToken: PropTypes.string,
    fetchIngredients: PropTypes.func.isRequired,
    ingredients: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        description: PropTypes.string,
        price: PropTypes.number,
        unit: PropTypes.string,
      })
    ),
    loading: PropTypes.bool,
  }
  static defaultProps = {
    ingredients: [],
    loading: true,
  }

  componentDidMount() {
    const { authToken, fetchIngredients } = this.props
    fetchIngredients(authToken)
  }

  renderIngredients() {
    const ingredients = this.props.ingredients || []

    return ingredients.map((ingredient, i) => (
      <IngredientItem key={`${ingredient.id}-${i}`} ingredient={ingredient} />
    ))
  }

  render() {
    if (this.props.loading) {
      return <div>Loading...</div>
    }
    return <div>{this.renderIngredients()}</div>
  }
}

export default IngredientsList
