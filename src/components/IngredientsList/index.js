import { compose } from 'redux'
import { connect } from 'react-redux'
import {
  getIngredients,
  getIsLoading,
} from 'src/redux/modules/ingredients/selectors'
import {
  fetchIngredients,
  updateIngredient,
} from 'src/redux/modules/ingredients/thunks'
import withAuthToken from 'src/redux/modules/users/withAuthToken'
import IngredientsList from './IngredientsList'

const mapStateToProps = state => ({
  ingredients: getIngredients(state),
  loading: getIsLoading(state),
})

const mapDispatchToProps = {
  fetchIngredients,
  updateIngredient,
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthToken
)(IngredientsList)
