import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setAuthToken } from 'src/redux/modules/users/thunks'
import Navbar from 'src/components/Navbar'
import 'src/index.css'

class Layout extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    setAuthToken: PropTypes.func.isRequired,
  }

  static defaultProps = {
    setAuthToken() { },
  }

  componentDidMount() {
    const authToken = window.localStorage.getItem('authToken') || null
    this.props.setAuthToken(authToken)
  }

  render() {
    const { children } = this.props
    return (
      <div className="container-sm w-screen">
        <Navbar />
        {children}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  authToken: state.users.authToken,
})

const mapDispatchToProps = {
  setAuthToken,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout)
