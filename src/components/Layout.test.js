import React from 'react'
import { render } from 'react-testing-library'
import Layout from './Layout'

jest.mock('src/redux/modules/users/thunks')

it('renders without crashing', () => {
  const { container } = render(<Layout.WrappedComponent />)
  console.log(container)
  expect(container).toBeTruthy()
})
