import React from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select'
import SubmitButtons from 'src/components/SubmitButtons'

const initialState = {
  name: '',
  description: '',
  ingredients: [],
  editing: false,
}

export default class RecipesForm extends React.Component {
  static propTypes = {
    authToken: PropTypes.string,
    createRecipe: PropTypes.func.isRequired,
    fetchIngredients: PropTypes.func.isRequired,
    finishEditRecipe: PropTypes.func.isRequired,
    ingredients: PropTypes.arrayOf(PropTypes.object),
    isEditing: PropTypes.bool.isRequired,
    updateRecipe: PropTypes.func.isRequired,
  }

  static defaultProps = {
    ingredients: [],
    editing: false,
  }

  state = initialState

  componentDidMount() {
    const { authToken, fetchIngredients } = this.props
    fetchIngredients(authToken)
  }

  getIngredientOptions() {
    const ingredients = this.props.ingredients || []
    return ingredients.map(ingredient => ({
      label: ingredient.name,
      value: ingredient.id,
    }))
  }

  getSelectedIngredients() {
    const ingredients = this.state.ingredients || []
    return ingredients.map(ingredient => ({
      label: ingredient.name,
      value: ingredient.id,
    }))
  }

  static getDerivedStateFromProps(newProps, prevState) {
    const hasStartedEditing = !prevState.editing && newProps.editingRecipe
    const hasFinishedEditing = prevState.editing && !newProps.editingRecipe

    if (hasStartedEditing) {
      const { name, description, ingredients } = newProps.editingRecipe

      return {
        name,
        description,
        ingredients,
        editing: true,
      }
    }

    if (hasFinishedEditing) {
      return initialState
    }

    return null
  }

  handleCancel = event => {
    event.preventDefault()
    this.props.finishEditRecipe()
    this.setState(initialState)
  }

  handleSubmit = event => {
    const { name, description, editing, ingredients } = this.state

    const {
      authToken,
      createRecipe,
      updateRecipe,
      isEditing,
    } = this.props

    event.preventDefault()

    if (isEditing) {
      updateRecipe(
        {
          name,
          description,
          id: editing.id,
          ingredients: ingredients.map(i => i.id),
        },
        authToken
      )
    } else {
      createRecipe(
        {
          name,
          description,
          ingredients: ingredients.map(i => i.id),
        },
        authToken
      )
    }

    this.setState(initialState)
  }

  handleFieldChange = event => {
    const name = event.target.name
    const value = event.target.value
    this.setState({ [name]: value })
  }

  handleSelectChange = selectedOptions => {
    const newSelectedIds = selectedOptions.map(selected => selected.value)
    const newSelected = this.props.ingredients.filter(ingredient =>
      newSelectedIds.includes(ingredient.id)
    )

    this.setState(prevState => ({
      ingredients: newSelected,
    }))
  }

  render() {
    const { name, description } = this.state

    return (
      <div className="form-container">
        <h3 className="pb-4">Recipes</h3>
        <div>
          <label htmlFor="name">Name</label>
          <input
            className="form-field"
            type="text"
            name="name"
            value={name}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="description">Description</label>
          <input
            className="form-field"
            type="textarea"
            name="description"
            onChange={this.handleFieldChange}
            value={description}
          />
        </div>
        <label htmlFor="ingredients">Ingredients</label>
        <Select
          className="mt-4"
          value={this.getSelectedIngredients()}
          onChange={this.handleSelectChange}
          name="ingredients"
          options={this.getIngredientOptions()}
          isMulti
          isSearchable
          placeholder="Add ingredients..."
        />
        <SubmitButtons
          onCancel={this.handleCancel}
          onSubmit={this.handleSubmit}
        />
      </div>
    )
  }
}
