import { connect } from 'react-redux'
import { signIn } from 'src/redux/modules/users/thunks'
import { selectors as userSelectors } from 'src/redux/modules/users'
import SignIn from './SignIn'

const mapStateToProps = state => ({
  signInError: userSelectors.getSignInError(state),
})

const mapDispatchToProps = {
  signIn,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn)
