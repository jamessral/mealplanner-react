import React from 'react'
import PropTypes from 'prop-types'
import SubmitButtons from 'src/components/SubmitButtons'
import { Link } from 'gatsby'

const initialState = {
  email: '',
  password: '',
}

export default class SignInForm extends React.PureComponent {
  state = initialState

  static propTypes = {
    signIn: PropTypes.func.isRequired,
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress)
  }

  get canSubmit() {
    const { email, password } = this.state
    const emailValid = email && email.length > 3
    const passwordValid = password && password.length > 3

    return Boolean(emailValid && passwordValid)
  }

  handleKeyPress = (event) => {
    // Enter key
    if (event.keyCode === 13) {
      this.handleSubmit(event)
    }
  }

  handleCancel = event => {
    console.log(event)
    event.preventDefault()
    this.setState({ ...initialState })
  }

  handleFieldChange = event => {
    const name = event.target.name
    const value = event.target.value

    this.setState({ [name]: value })
  }

  handleSubmit = event => {
    event.preventDefault()
    const { email, password } = this.state
    this.props.signIn(email, password)
    this.setState({ ...initialState })
  }

  render() {
    const { email, password } = this.state

    return (
      <div className="form-container">
        <h3 className="pb-4">Sign In</h3>
        <form>
          <label htmlFor="email">Email</label>
          <input
            className="form-field"
            type="email"
            name="email"
            value={email}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="password">Password</label>
          <input
            className="form-field"
            type="password"
            name="password"
            value={password}
            onChange={this.handleFieldChange}
          />
          <SubmitButtons
            onCancel={this.handleCancel}
            onSubmit={this.handleSubmit}
            disabled={!this.canSubmit}
          />
        </form>
        <div className="flex flex-row justify-between items-center mt-4">
          <span>Don't have an account?</span>
          <Link className="text-blue text-base font-bold" to="/sign_up">
            Sign Up
          </Link>
        </div>
      </div>
    )
  }
}
