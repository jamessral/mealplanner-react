import { compose } from 'redux'
import { connect } from 'react-redux'
import { withAuthToken } from 'src/redux/modules/users'
import {
  selectors as ingredientSelectors,
  thunks as ingredientThunks,
} from 'src/redux/modules/ingredients'
import IngredientsForm from './IngredientsForm'

const mapStateToProps = state => ({
  isEditing: ingredientSelectors.getIsEditing(state),
})

const mapDispatchToProps = {
  createIngredient: ingredientThunks.createIngredient,
  finishEditIngredient: ingredientThunks.finishEditIngredient,
  updateIngredient: ingredientThunks.updateIngredient,
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withAuthToken
)(IngredientsForm)
