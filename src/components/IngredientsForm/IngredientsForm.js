import React from 'react'
import PropTypes from 'prop-types'
import SubmitButtons from 'src/components/SubmitButtons'

const initialState = {
  name: '',
  description: '',
  price: 0,
  unit: '',
  editing: false,
}

export default class IngredientsForm extends React.Component {
  static propTypes = {
    authToken: PropTypes.string,
    createIngredient: PropTypes.func.isRequired,
    isEditing: PropTypes.bool.isRequired,
    finishEditIngredient: PropTypes.func.isRequired,
    updateIngredient: PropTypes.func.isRequired,
  }

  static defaultProps = {
    isEditing: false,
  }

  state = initialState

  static getDerivedStateFromProps(newProps, prevState) {
    const hasStartedEditing = !prevState.editing && newProps.editingIngredient

    const hasFinishedEditing = prevState.editing && !newProps.editingIngredient

    if (hasStartedEditing) {
      const { name, description, price, unit } = newProps.editingIngredient

      return {
        name,
        description,
        price,
        unit,
        editing: true,
      }
    }

    if (hasFinishedEditing) {
      return initialState
    }

    return null
  }

  handleCancel = event => {
    event.preventDefault()
    this.props.finishEditIngredient()
    this.setState(initialState)
  }

  handleSubmit = event => {
    const { name, description, price, editing, unit } = this.state

    const {
      authToken,
      createIngredient,
      isEditing,
      updateIngredient,
    } = this.props

    event.preventDefault()

    if (isEditing) {
      updateIngredient(
        {
          name,
          description,
          price: Number(price),
          id: editing.id,
          unit,
        },
        authToken
      )
    } else {
      createIngredient(
        {
          name,
          description,
          price: Number(price),
        },
        authToken
      )
    }

    this.setState(initialState)
  }

  handleFieldChange = event => {
    const name = event.target.name
    const value = event.target.value
    this.setState({ [name]: value })
  }

  render() {
    const { name, description, price, unit } = this.state
    return (
      <div className="form-container">
        <h3 className="pb-4">Ingredients</h3>
        <div>
          <label htmlFor="name">Name</label>
          <input
            className="form-field"
            type="text"
            name="name"
            value={name}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="description">Description</label>
          <input
            className="form-field"
            type="textarea"
            name="description"
            onChange={this.handleFieldChange}
            value={description}
          />
          <label htmlFor="price">Price</label>
          <input
            className="form-field"
            type="number"
            name="price"
            value={price}
            onChange={this.handleFieldChange}
          />
          <label htmlFor="unit">Unit</label>
          <input
            className="form-field"
            type="text"
            name="unit"
            value={unit}
            onChange={this.handleFieldChange}
          />
        </div>
        <SubmitButtons
          onCancel={this.handleCancel}
          onSubmit={this.handleSubmit}
        />
      </div>
    )
  }
}
