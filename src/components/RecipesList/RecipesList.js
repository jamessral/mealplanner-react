import React from 'react'
import PropTypes from 'prop-types'
import RecipeItem from './RecipeItem'

class RecipesList extends React.Component {
  static propTypes = {
    authToken: PropTypes.string,
    fetchRecipes: PropTypes.func.isRequired,
    recipes: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        description: PropTypes.string,
        price: PropTypes.number,
      })
    ),
    loading: PropTypes.bool,
  }
  static defaultProps = {
    recipes: [],
    loading: true,
  }

  componentDidMount() {
    const { authToken, fetchRecipes } = this.props
    fetchRecipes(authToken)
  }

  renderRecipes() {
    return this.props.recipes.map((recipe, i) => (
      <RecipeItem key={`${recipe.id}-${i}`} recipe={recipe} />
    ))
  }

  render() {
    if (this.props.loading) {
      return <div>Loading...</div>
    }
    return <div>{this.renderRecipes()}</div>
  }
}

export default RecipesList
