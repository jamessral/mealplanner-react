import React from 'react'
import PropTypes from 'prop-types'

class RecipeItem extends React.Component {
  static propTypes = {
    beginEditRecipe: PropTypes.func.isRequired,
    recipe: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string,
    }).isRequired,
    removeRecipe: PropTypes.func.isRequired,
  }

  handleDelete = () => {
    const { recipe, removeRecipe } = this.props

    removeRecipe(recipe.id)
  }

  handleEdit = () => {
    const { beginEditRecipe, recipe } = this.props
    beginEditRecipe(recipe.id)
  }

  render() {
    const { recipe } = this.props
    return (
      <div className="flex flex-col justify-center px-4 py-4 shadow-md rounded">
        <div className="flex justify-between">
          <div className="text-grey-darkest text-lg font-bold py-2">
            Name: <span className="font-normal text-base">{recipe.name}</span>
          </div>
          <button className="text-grey-dark" onClick={this.handleDelete}>
            X
          </button>
        </div>
        <div className="text-grey-darkest text-lg font-bold py-2">
          Description:{' '}
          <span className="font-normal text-base">{recipe.description}</span>
        </div>
        <button onClick={this.handleEdit} className="btn btn-blue-rev">
          Edit
        </button>
      </div>
    )
  }
}

export default RecipeItem
