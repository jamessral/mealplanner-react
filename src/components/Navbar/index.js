import { connect } from 'react-redux'
import { getIsSignedIn } from 'src/redux/modules/users/selectors'
import { signOut } from 'src/redux/modules/users/thunks'
import Navbar from './Navbar'

const mapStateToProps = state => ({
  isSignedIn: getIsSignedIn(state),
})

const mapDispatchToProps = {
  signOut,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar)
