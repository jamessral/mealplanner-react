import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

export default class Navbar extends React.PureComponent {
  static propTypes = {
    isSignedIn: PropTypes.bool,
    signOut: PropTypes.func.isRequired,
  }

  static defaultProps = {
    isSignedIn: false,
  }

  handleSignOut = event => {
    const { signOut } = this.props
    event.preventDefault()
    signOut()
  }

  render() {
    const { isSignedIn } = this.props

    return (
      <div className="bg-teal px-6 py-4 flex flex-wrap justify-between">
        <div className="flex flex-1 items-center text-white pr-6 pr-8">
          <Link className="home-link " to="/">
            MealPlanner
          </Link>
        </div>
        {isSignedIn && (
          <div className="flex flex-1 items-center justify-between p-0">
            <Link
              className="text-teal-lighter hover:text-white"
              to="ingredients"
            >
              Ingredients
            </Link>
            <Link className="text-teal-lighter hover:text-white" to="recipes">
              Recipes
            </Link>
            <Link className="text-teal-lighter hover:text-white" to="mealplans">
              MealPlans
            </Link>
            <button className="link" onClick={this.handleSignOut}>
              Sign Out
            </button>
          </div>
        )}
        {!isSignedIn && (
          <Link className="link" to="sign_in">
            Sign In
          </Link>
        )}
      </div>
    )
  }
}
