import React from 'react'
import Layout from 'src/components/Layout'
import SignIn from 'src/components/SignIn'

export default () => (
  <Layout>
    <SignIn />
  </Layout>
)
