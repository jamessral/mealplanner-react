import React from 'react'
import Layout from 'src/components/Layout'
import RecipesForm from 'src/components/RecipesForm'
import RecipesList from 'src/components/RecipesList'

export default () => (
  <Layout>
    <RecipesForm />
    <RecipesList />
  </Layout>
)
