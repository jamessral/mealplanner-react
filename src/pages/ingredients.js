import React from 'react'
import Layout from 'src/components/Layout'
import IngredientsForm from 'src/components/IngredientsForm'
import IngredientsList from 'src/components/IngredientsList'

const Ingredients = () => (
  <Layout>
    <IngredientsForm />
    <IngredientsList />
  </Layout>
)

export default Ingredients
