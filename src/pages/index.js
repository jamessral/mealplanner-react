import React from 'react'
import Layout from 'src/components/Layout'
import registerServiceWorker from 'src/registerServiceWorker'

export default () => <Layout>Welcome to the Mealplanner</Layout>
registerServiceWorker()
