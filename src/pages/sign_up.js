import React from 'react'
import Layout from 'src/components/Layout'
import SignUp from 'src/components/SignUp'

export default () => (
  <Layout>
    <SignUp />
  </Layout>
)
