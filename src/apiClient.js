import axios from 'axios'

const port = process.env.APIPORT || 3000

export default axios.create({
  baseURL: `http://localhost:${port}/api/v1`,
  timeout: 1000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})
