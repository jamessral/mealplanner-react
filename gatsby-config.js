const path = require('path')

module.exports = {
  siteMetadata: {
    title: "James' Mealplanner",
  },
  plugins: [
    'gatsby-plugin-eslint',
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        src: path.join(__dirname, 'src'),
        pages: path.join(__dirname, 'src/pages'),
      },
    },
  ],
}
