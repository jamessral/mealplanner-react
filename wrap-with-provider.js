import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import store from 'src/redux/store'

const Wrapper = ({ element }) => <Provider store={store}>{element}</Provider>

Wrapper.propTypes = {
  element: PropTypes.node,
}

export default Wrapper
